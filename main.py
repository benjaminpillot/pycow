# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import os

from pycow.reader.inmet import get_list_of_stations
from pycow.base.station import Station

directory = "/home/benjamin/Documents/PRO/PROJET_INMET_FRED_SATGE/001_DONNEES/INMET/RAW_DATA"
out_dir = "/home/benjamin/Documents/PRO/PROJET_INMET_FRED_SATGE/001_DONNEES/DATA_MARIE_PAULE"

stations_jurua = get_list_of_stations(directory, ["A108", "A136", "A137", "A109", "A138"])

for _, row in stations_jurua.iterrows():
    station = Station(row["Latitude"], row["Longitude"], row["Altitude"], row["ID"]).from_inmet(
        directory=directory)
    # test = _enhanced_sigma_test(station.observations["temperature"].data)
    # station.to_file("swat", directory=out_dir)
    # station = station.from_inmet(directory=directory)
    new_station = station.resample_to("D", min_count=20)
    new_station.to_file("swat", directory=out_dir)


stations_jurua.to_csv(os.path.join(out_dir, "list_of_stations_in_jurua.txt"),
                      sep=",", float_format="%.3f", index=False)
