# -*- coding: utf-8 -*-

""" All tools related to units (see pint package)

More detailed description.
"""
from pint import UnitRegistry


class UnitConverter:
    
    def __init__(self, from_, to_):
        """ Class instance constructor

        Parameters
        ----------
        from_: str
            unit string (must be readable by pint)
        to_: str
            unit string (must be readable by pint)
        """
        self._ureg = UnitRegistry()
        self.unit_from = from_
        self.unit_to = to_

    def __call__(self, value, units):
        """ Convert value from one unit to another

        Parameters
        ----------
        value: int or float or numpy.ndarray
        units: str

        Returns
        -------

        """
        units = self._ureg(units).units

        if units == self.unit_from.units:
            return value * self.conversion_factor
        elif units == self.unit_to.units:
            return value / self.conversion_factor
        else:
            return None
    
    @property
    def conversion_factor(self):

        if not (self.unit_from / self.unit_to).dimensionless:
            raise ValueError("Both units must be homogeneous")

        conversion_factor = \
            self.unit_from.to_base_units() / self.unit_to.to_base_units()
        
        return conversion_factor.magnitude

    @property
    def unit_from(self):
        return self._unit_from

    @unit_from.setter
    def unit_from(self, unit):
        self._unit_from = self._ureg(unit)

    @property
    def unit_to(self):
        return self._unit_to

    @unit_to.setter
    def unit_to(self, unit):
        self._unit_to = self._ureg(unit)
