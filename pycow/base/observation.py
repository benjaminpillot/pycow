# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
from pycow.tools.io import _to_csv
from pycow.tools.resampling import _resample_to


class Observation:
    """ Weather station observation

    """

    _q_flags = None

    def __init__(self, data, name=None, unit=None):
        """ Build station observation class instance

        Parameters
        ----------
        data: pd.Series
            time series of values
        name: str
            observation name
        unit: str
            observation unit
        """

        self._data = data
        self.name = name
        self.unit = unit

    def resample_to(self, rule, resampler, min_count=-1):
        """ Resample observation time series to new frequency

        Parameters
        ----------
        rule: str
            valid offset alias, such as:
                H (hourly frequency)
                D (calendar day frequency)
                W (weekly frequency)
                M (month end frequency)
                Y (year end frequency).
            see https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#offset-aliases.
        resampler: str or function
            valid resampler function (sum, mean, etc. or custom function)
        min_count: int
            required number of valid values to perform resampling


        Returns
        -------
        Observation
            An Observation instance of new resampled values

        """
        return _resample_to(self, rule, resampler, min_count)

    def to_csv(self, filename, fmt, float_format="%.2f"):
        """ Write observation to csv file

        Parameters
        ----------
        filename: str
            file name
        fmt: str
            valid format {'swat', etc.}  # TODO implements csv formats
        float_format: str, Default "%.2f"
            float format

        Returns
        -------

        """
        return _to_csv(self, filename, fmt, float_format)

    @property
    def q_flags(self):
        return self._q_flags

    @property
    def data(self):
        return self._data
