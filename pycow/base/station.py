# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import os

from pycow.reader.inmet import _from_inmet
from pycow.tools.io import _to_file
from pycow.tools.resampling import _resample_station
from pycow.utils import HybridMethod


class Station:
    """ Weather station class

    """
    format = None

    def __init__(self, latitude, longitude, altitude=None, name=None):
        """ Build Station class instance

        Parameters
        ----------
        latitude: float
            station latitude
        longitude: float
            station longitude
        altitude: float
            station altitude
        """
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.name = name
        self.observations = {}

    def from_(self, filename, reader):
        """ Retrieve station instance

        Parameters
        ----------
        filename: str
            filename from which data must be read
        reader: pyrcows.reader.core.Reader
            Reader instance used for reading formatted data

        Returns
        -------

        """
        pass

    @HybridMethod
    def from_inmet(self, directory, name=None):
        """ Retrieve station instance from INMET data files

        This method can be called with class or instance. Better
        to be called from instance for performance reasons, that
        is first extract the whole list of stations and build them
        one by one using the Station class.

        Parameters
        ----------
        directory: str
            path to directory where INMET files are stored
        name: str
            INMET station name if method called with class

        Returns
        -------

        """
        return _from_inmet(self, directory, name)

    def quality_check(self):
        pass

    def resample_to(self, rule, min_count=-1):
        """ Resample observations

        Parameters
        ----------
        rule: str
            valid offset alias, such as:
                H (hourly frequency)
                D (calendar day frequency)
                W (weekly frequency)
                M (month end frequency)
                Y (year end frequency).
            see https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#offset-aliases.
        min_count: int
            required number of valid values to perform resampling

        Returns
        -------
        Station

        """
        return _resample_station(self, rule, min_count)

    def to_file(self, fmt, directory=os.getcwd(), float_format="%.2f"):
        """

        Parameters
        ----------
        fmt: str
            csv conversion format
        directory: str, Default current directory
            directory path
        float_format: str
            float format

        Returns
        -------

        """
        return _to_file(self, directory, fmt, float_format)


if __name__ == "__main__":
    # test = Station(42, 30, 250, "porto_alegre").from_inmet(
    #     "/home/benjamin/Documents/PROJET_INMET_FRED_SATGE/001_DONNEES/INMET/RAW_DATA")
    test = Station.from_inmet(directory="/home/benjamin/Documents/PRO/PROJET_INMET_"
                                        "FRED_SATGE/001_DONNEES/INMET/RAW_DATA",
                              name="porto_alegre").resample_to('D')
    print(type(test))
    print(test.temperature.data)
