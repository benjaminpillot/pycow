# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
from copy import deepcopy
from functools import wraps


def _return_observation(function):
    @wraps(function)
    def return_observation(observation, *args, **kwargs):
        data = function(observation, *args, **kwargs)
        new_observation = observation.__class__(data, name=observation.name)

        return new_observation
    return return_observation


def _return_station(method):
    @wraps(method)
    def return_station(self, *args, **kwargs):
        new_station = deepcopy(self)
        new_station = method(new_station, *args, **kwargs)

        return new_station

    return return_station
