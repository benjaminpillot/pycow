# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import os
import re

import numpy as np
import pandas as pd
from tqdm import tqdm
from unidecode import unidecode

from pycow.base.observation import Observation
from pycow.utils import find_file, get_altitude, get_decimal_coordinate

RE_PATTERN = r"__([A-Za-z0-9]+)_([A-Z0-9]+)_([A-Z_\.]+?)_?([a-z]+?)?_?\.[a-z]"
HEADER_HEIGHT = 9
TIME_ROW_IDX = 2
TIME_COL_IDX = 0
HOUR_ROW_IDX = 1
HOUR_SCALE_FACTOR = 100
LAST_DAY_OFFSET = "23h"  # Offset for including the hours of the last day
TIME_STEP = 'H'  # INMET data have hourly time step
FORMAT = dict(temperature=dict(name="TEMPERATURA DO AR", unit="°C", resampler="mean"),
              humidity=dict(name="UMIDADE RELATIVA DO AR", unit="%", resampler="mean"),
              temperature_max=dict(name="TEMPERATURA MAXIMA", unit="°C", resampler="mean"),
              temperature_min=dict(name="TEMPERATURA MINIMA", unit="°C", resampler="mean"),
              wind_speed=dict(name="VENTO VELOCIDADE", unit="m/s", resampler="mean"),
              radiation=dict(name="RADIACAO", unit="kJ/m²", resampler="sum"),
              rainfall=dict(name="PRECIPITACAO", unit="mm", resampler="sum"),
              pressure=dict(name="PRESSAO ATMOSFERICA", unit="hPa", resampler="mean"),
              pressure_max=dict(name="PRESSAO ATMOSFERICA MAXIMA", unit="hPa", resampler="mean"),
              pressure_min=dict(name="PRESSAO ATMOSFERICA MINIMA", unit="hPa", resampler="mean"))


def get_list_of_stations(directory, extension=".xls", sort_by="ID"):
    """ Get full list of stations in directory as a pandas dataframe

    Returns
    -------
    pd.DataFrame:
        DataFrame of station characteristics (name, location, code, etc.)
    """
    try:
        list_of_files = find_file(extension, directory)
    except TypeError:
        list_of_files = list(set([item for ext in extension
                                  for item in find_file(ext, directory)]))
    id_, name, state = [], [], []
    altitude, latitude, longitude = [], [], []

    for file in tqdm(list_of_files, desc="Extract INMET station(s) info"):
        filename = os.path.basename(file)

        try:
            re_match = re.match(RE_PATTERN, filename).groups()
            station_id = re_match[1]

            if station_id not in id_:

                id_.append(station_id)
                state.append(re_match[0])
                name.append(re_match[2])
                df = pd.read_excel(file, header=None).iloc[0:10, :]

                for n in range(len(df)):
                    row = df.iloc[n].dropna().reset_index(drop=True)
                    if row.size != 0:
                        try:
                            if row[0].lower()[:3] == "alt":
                                altitude.append(get_altitude(row[1]))
                            elif row[0].lower()[:3] == "lat":
                                latitude.append(get_decimal_coordinate(row[1]))
                            elif row[0].lower()[:3] == "lon":
                                longitude.append(get_decimal_coordinate(row[1]))
                        except AttributeError:
                            pass

        except AttributeError:
            pass

    id_ = pd.Series(id_, name="ID")
    name = pd.Series(name, name="Name")
    state = pd.Series(state, name="State")
    latitude = pd.Series(latitude, name="Latitude")
    longitude = pd.Series(longitude, name="Longitude")
    altitude = pd.Series(altitude, name="Altitude")

    df = pd.concat([id_, name, state, latitude, longitude, altitude], axis=1)

    return df.sort_values(by=sort_by, ignore_index=True)


def _from_inmet(station, directory, name=None):
    """ Build Station instance from INMET data

    Parameters
    ----------
    station
    directory
    name

    Returns
    -------

    """
    try:
        station.format = FORMAT
        observations = _inmet_import(directory, station.name)
        for observation in observations:
            station.observations[observation.name] = observation
    except AttributeError:
        ws_info = get_list_of_stations(directory, name.upper())
        return _from_inmet(station(ws_info["Latitude"], ws_info["Longitude"],
                                   ws_info["Altitude"], name), directory)

    return station


def _inmet_import(directory, name):
    """ Import time series from INMET files in directory

    Parameters
    ----------
    directory: str
        path to directory where INMET files are stored
    name: str
        station name

    Returns
    -------
    list
        list of Observation instances

    """
    def inmet_import():
        inmet_timeseries = dict()
        inmet_files = find_file(name, directory)
        for file in tqdm(inmet_files, desc="Reading data from station '%s'" % name):
            collection = _read_inmet_data(file, FORMAT.keys())
            for key, value in collection.items():
                if key in inmet_timeseries.keys():
                    inmet_timeseries[key] = pd.concat([inmet_timeseries[key], collection[key]])
                else:
                    inmet_timeseries[key] = collection[key]

        return inmet_timeseries

    return [Observation(series, name=key, unit=FORMAT[key]["unit"])
            for key, series in inmet_import().items()]


def _get_inmet_data(inmet_database, obs_name):
    """ Get INMET data in data frame

    Read specific data in INMET file and return corresponding time series

    Parameters
    ----------
    inmet_database: DataFrame
        INMET data frame
    obs_name: str
        observation name (temperature, temperature_min, temperature_max, humidity,
        wind_speed, rainfall, radiation, pressure, pressure_min, pressure_max)

    Returns
    -------

    """
    selected_col = [FORMAT[obs_name]["name"] in unidecode(cell) if isinstance(cell, str)
                    else False for cell in inmet_database.iloc[0]]
    obs_database = inmet_database.loc[:, selected_col]

    if obs_database.empty:
        raise ValueError("Variable '%s' not in database" % obs_name)

    inmet_hours = obs_database.iloc[HOUR_ROW_IDX, :]
    days = inmet_database.iloc[TIME_ROW_IDX:len(inmet_database), TIME_COL_IDX]
    days_with_last_edge = np.concatenate([days, [days.iloc[-1] + pd.Timedelta(LAST_DAY_OFFSET)]])
    obs_series = pd.Series(index=days_with_last_edge,
                           data=0, dtype='float64').resample(TIME_STEP).asfreq(0)

    for idx in range(inmet_hours.size):
        selected_hour = days + pd.Timedelta(hours=inmet_hours.iloc[idx] / HOUR_SCALE_FACTOR)
        obs_series.loc[selected_hour] = \
            obs_database.iloc[TIME_ROW_IDX::, idx].values.astype('float')

    return obs_series


def _read_inmet_data(filename, obs_names):
    """ Read INMET data from file

    Parameters
    ----------
    filename: str
        INMET excel file
    obs_names: list or tuple
        Collection of valid observation names

    Returns
    -------
    Dictionary of var timeseries

    """
    inmet_database = pd.read_excel(filename, header=None, skiprows=HEADER_HEIGHT)
    obs_collection = dict()

    for obs_name in obs_names:
        try:
            obs_collection[obs_name] = _get_inmet_data(inmet_database, obs_name)
        except ValueError:
            pass

    return obs_collection
