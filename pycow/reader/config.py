# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import importlib.util
import re
import warnings
from configparser import ConfigParser

from pycow.reader import DateParser

LIST_SEP = ","


def _parse_config(reader, filepath):
    """ Parse config file

    Parameters
    ----------
    reader
    filepath

    Returns
    -------

    """
    config = RConfigParser()
    config.read(filepath)


class RConfigParser(ConfigParser):
    GEN_SEC = "GENERAL"

    DATE_COL = 0
    DATE_FORMAT = None
    DATE_PARSER = DateParser
    DECIMAL = '.'
    HEADER = None
    NA_FILTER = True
    NA_VALUES = None
    SEPARATOR = None
    SKIP_BLANK_LINES = True

    def _get_bool(self, option, default):
        string = self.get(self.GEN_SEC, option)
        if string:
            return bool(string)
        else:
            return default

    def _get_list_of_int(self, option, default):
        string = re.sub(r"[^\d,]", '', self.get(self.GEN_SEC, option))
        if string:
            try:
                return [int(value) for value in string.split(sep=LIST_SEP)]
            except ValueError:
                raise ValueError(f"{option.upper()} must be an int or list of int")
        else:
            return default

    def _get_list_of_str(self, option, default):
        string = self.get(self.GEN_SEC, option)
        if string:
            return string.split(sep=LIST_SEP)
        else:
            return default

    def _get_module(self):
        module_path = self.get(self.GEN_SEC, "user_module_path")
        if module_path:
            spec = importlib.util.spec_from_file_location("user_module", module_path)
            try:
                user_module = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(user_module)
            except (AttributeError, FileNotFoundError):
                raise ValueError(f"Impossible to load module from file '{module_path}'")
            else:
                return user_module

    def _get_str(self, option, default):
        string = self.get(self.GEN_SEC, "separator")
        if string:
            return string
        else:
            return default

    @property
    def date_col(self):
        return self._get_list_of_int("date_col", self.DATE_COL)

    @property
    def date_format(self):
        date_format = self.get(self.GEN_SEC, "date_format")
        if date_format:
            return date_format
        else:
            return self.DATE_FORMAT

    @property
    def date_parser(self):
        date_parser = self.get(self.GEN_SEC, "date_parser")
        if self._get_module():
            try:
                return getattr(self._get_module(), date_parser)
            except TypeError:
                return self.DATE_PARSER
            except AttributeError:
                raise ValueError(f"Unknown date parser '{date_parser}'")
        else:
            if date_parser:
                warnings.warn("No module path given, fall back to default date_parser")

            return self.DATE_PARSER

    @property
    def header(self):
        return self._get_list_of_int("header", self.HEADER)

    @property
    def na_filter(self):
        return self._get_bool("na_filter", self.NA_FILTER)

    @property
    def na_values(self):
        return self._get_list_of_str("na_values", self.NA_VALUES)

    @property
    def separator(self):
        return self._get_str("separator", self.SEPARATOR)

    @property
    def skip_blank_lines(self):
        return self._get_bool("skip_blank_lines", self.SKIP_BLANK_LINES)
