# -*- coding: utf-8 -*-

""" All classes and tools related to building station readers

More detailed description.
"""
from abc import abstractmethod

from pycow.reader.parser import DateParser
from pycow.utils import must_be, protected_property


class Reader:

    date_col = protected_property("date_col")
    date_parser = protected_property("date_parser")
    header = protected_property("header")
    separator = protected_property("separator")

    def __init__(self, date_col=0, date_format=None, header=None,
                 date_parser=DateParser, separator=None):
        """ Reader class constructor

        Parameters
        ----------
        date_col
        date_format
        header
        date_parser
        separator
        """
        self.date_col = date_col
        self.header = header
        self.date_parser = date_parser(date_format)
        self.separator = separator
        self._observations = {}

    @abstractmethod
    def __call__(self, filename):
        pass

    def add_observation(self, name, col_name=None, col_pos=None, unit=None, resampler=None):
        """ Add observation to read from file

        Parameters
        ----------
        name: str
            observation name
        col_name: str
            column name of the observation in the
            data file (if there is no name, set None)
        col_pos: int
            column position of the observation (if name is not set,
            observation will be retrieved using the column position)
        unit: str
            observation unit as a valid string (see pint package)
        resampler: str or function
            valid resampler function (sum, mean, etc. or custom function)
            that should be used when resampling observation

        Returns
        -------

        """
        self._observations[name] = dict(name=col_name,
                                        col_idx=col_pos,
                                        unit=unit,
                                        resampler=resampler)

    def parse_config(self, filepath):
        """ Build Reader attributes from Python config file

        Parameters
        ----------
        filepath: str
            path to config file

        Returns
        -------

        """
        pass

    @property
    def engine(self):
        if self._separator is not None:
            return "c"
        else:
            return "python"

    @property
    def observations(self):
        return self._observations

    @date_col.setter
    @must_be((int, (list, tuple)), non_negative=True)
    def date_col(self, value):
        self._date_col = value

    @date_parser.setter
    @must_be(DateParser)
    def date_parser(self, parser):
        self._date_parser = parser

    @header.setter
    @must_be((int, (list, tuple)), non_negative=True, can_be_none=True)
    def header(self, value):
        self._header = value

    @separator.setter
    @must_be(str, can_be_none=True)
    def separator(self, sep):
        self._separator = sep


class CsvReader(Reader):

    def __init__(self):
        super().__init__()

    def __call__(self, filename):
        pass


class XlsReader(Reader):

    def __init__(self):
        super().__init__()

    def __call__(self, filename):
        pass
