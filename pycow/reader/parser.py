# -*- coding: utf-8 -*-

""" All tools for parsing

More detailed description.
"""
import datetime

import dateutil.parser

from pycow.exceptions import DateParserError
from pycow.utils import must_be, protected_property


class DateParser(dateutil.parser.parser):

    date_format = protected_property("date_format")

    def __init__(self, date_format=None):
        super().__init__()
        self.date_format = date_format

    @date_format.setter
    @must_be(str, can_be_none=True)
    def date_format(self, value):
        self._date_format = value

    def __call__(self, date_string):

        if self._date_format:
            try:
                return datetime.datetime.strptime(date_string, self._date_format)
            except ValueError as e:
                raise DateParserError(e)
        else:
            return self.parse(date_string)
