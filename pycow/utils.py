# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import os
import re
from collections import Collection
from functools import wraps


class HybridMethod:
    def __init__(self, func):
        self.func = func

    def __get__(self, obj, cls):
        context = obj if obj is not None else cls

        @wraps(self.func)
        def hybrid(*args, **kw):
            return self.func(context, *args, **kw)

        return hybrid


def find_file(file, directory=os.getcwd(), sort=True):
    """ Find file(s) in directory

    Description
    -----------

    Parameters
    ----------
    file: str
        file name/extension
    directory: str
        relative/absolute path to directory
    sort: bool
        sort resulting list

    Returns
    -------
    list of file paths: list

    :Example:
        * Look for file extension in current directory
        >>> find_file(".py")
        ["/path/to/file.py", "/path/to/file2.py", ...]

        * Look for file name(s) in specific directory
        >>> find_file("filename", "/path/to/valid/directory")
        ["path/to/this-is-a-filename.txt", "path/to/filename.ods", ...]

        * Look for multiple file names/extension in current directory
        >>> list(set([item for item in find_file(...) for ... in ["filename", ".py"]]))

    """

    result = []

    if file[0] == ".":  # pattern: file extension
        pattern = r'[^\\/:*?"<>|\r\n]+' + re.escape(file) + '$'
    else:  # pattern: file name (special regex characters are regarded as literal)
        pattern = re.escape(file)

    if os.path.isdir(directory):
        for root, dirs, files in os.walk(directory):
            for name in files:
                if re.search(pattern, name) is not None:
                    result.append(os.path.join(root, name))
    else:
        raise ValueError("'{}' is not a valid directory path".format(directory))

    if sort:
        result = sorted(result)

    return result


def get_altitude(alt_str):

    return get_float(re.match(r"(.+)m", alt_str)[1])


def get_float(float_str):

    return float(float_str.replace(",", "."))


def get_decimal_coordinate(coord_str):
    """ Convert coordinate string into decimal coordinate

    Description
    -----------

    Parameters
    ----------

    coord_str: str
        coordinate string in degrees and minutes

    Returns
    -------
    coordinate: float
        corresponding decimal latitude
    """

    match = re.match(r"(\d+)°(\d+)'([A-Z])", coord_str)
    coord = float(match[1]) + float(match[2]) / 60

    if match[3] == 'S' or match[3] == 'W':
        return - coord
    else:
        return coord


def lazyproperty(func):
    name = '_lazy_' + func.__name__

    @property
    def lazy(self):
        if hasattr(self, name):
            return getattr(self, name)
        else:
            value = func(self)
            setattr(self, name, value)
            return value
    return lazy


def must_be(_type, can_be_none=None, non_negative=None,
            positive=None, valid_values=None):
    """ Setter method decorator

    Parameters
    ----------
    _type: type or tuple[type]
        if value can be a collection of specific types,
        define _type as for instance: _type = (int, (list, tuple))
        which means here that value might be an integer or a
        collection of integers
    can_be_none: bool
    non_negative: bool
    positive: bool
    valid_values

    Returns
    -------

    """

    def decorator(setter):
        @wraps(setter)
        def wrapper(self, value):

            _value = value

            # Type check
            if can_be_none:
                fmt = (_type, type(None))
            else:
                fmt = _type
            if not isinstance(_value, fmt):
                raise TypeError(f"'{setter.__name__}' must be '%s' but is: '%s'" %
                                (_type, type(_value)))
            if isinstance(_value, (list, tuple)):
                if not all(isinstance(val, _type[0]) for val in _value):
                    raise TypeError(f"all values in '{setter.__name__}' collection "
                                    f"must be {_type[0]}")

            # Value check
            if valid_values is not None:
                if _value not in valid_values:
                    raise ValueError(f"'{setter.__name__}' must match one of those: "
                                     f"{valid_values}")

            if non_negative is not None and _value is not None:
                if not isinstance(_value, Collection):
                    _value = [_value]
                if non_negative is True:
                    if any(val < 0 for val in _value):
                        raise ValueError(f"'{setter.__name__}' value(s) must be >= 0")
                else:
                    if any(val >= 0 for val in _value):
                        raise ValueError(f"'{setter.__name__}' value(s) must be < 0")
            if positive is not None and _value is not None:
                if not isinstance(_value, Collection):
                    _value = [_value]
                if positive is True:
                    if any(val <= 0 for val in _value):
                        raise ValueError(f"'{setter.__name__}' value(s) must be > 0")
                else:
                    if any(val > 0 for val in _value):
                        raise ValueError(f"'{setter.__name__}' value(s) must be <= 0")

            output = setter(self, value)

        return wrapper
    return decorator


def protected_property(name):
    """ Protected property

    Function used for implementing
    repetitive "SetAccess = protected"
    of class properties
    :param name:
    :return:
    """
    storage_name = '_' + name

    @property
    def prop(self):
        return getattr(self, storage_name)

    return prop

