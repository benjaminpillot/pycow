# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""

import numpy as np

from pycow.base import _return_observation, _return_station


@_return_observation
def _resample_to(observation, rule, resampler, min_count):
    """

    Parameters
    ----------
    observation: pyrcows.observation.Obsevation
        Observation instance to resample
    rule: str
        valid offset alias
    resampler: str or function
        valid resampler function
    min_count: int
        required number of valid values to perform resampling

    Returns
    -------
    pandas.Series

    """
    resampled_observation = observation.data.resample(rule).apply(resampler)
    resampled_observation[observation.data.resample(rule).count() < min_count] = np.nan

    return resampled_observation
    # else:
    #     resampled_data = observation.data.copy()
    #     valid_values = np.logical_or.reduce([(observation.q_flags == flag)
    #                                          for flag in valid_qflags])
    #     resampled_data.iloc[~valid_values] = np.nan
    #
    #     return resampled_data.resample(rule).apply(resampler, min_count=min_count)


@_return_station
def _resample_station(station, rule, min_count):
    """

    Parameters
    ----------
    station
    rule
    min_count

    Returns
    -------

    """
    for obs_name, observation in station.observations.items():
        resampler = station.format[obs_name]["resampler"]
        station.observations[obs_name] = observation.resample_to(rule, resampler, min_count)

    return station
