# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import os

import numpy as np


def _to_csv(observation, filename, fmt, float_format):
    """ Write to csv file using given format

    Parameters
    ----------
    observation: pyrcows.observation.Observation
        observation to convert/write
    filename: str
        valid csv file name
    fmt: str
        valid format {'swat', }
    float_format: str
        float format (e.g. "%.2f")

    Returns
    -------

    """

    def to_format():
        """ Build dictionary of format-writing functions

        """
        return dict(swat=to_swat())

    def to_swat():
        """ Write to csv file using SWAT format

        """
        np.savetxt(filename, observation.data.values, fmt=float_format,
                   header=observation.data.index[0].strftime("%Y%m%d"), comments='')

        return 0

    return to_format()[fmt]


def _to_file(station, directory, fmt, float_format):
    """ Write station observations to file

    Parameters
    ----------
    station: pyrcows.station.Station
    directory: str
        valid directory path
    fmt: str
        valid format for conversion to csv
    float_format: str
        float format (e.g. "%.2f")

    Returns
    -------

    """
    for obs_name, observation in station.observations.items():
        filename = os.path.join(directory, station.name + "_" + obs_name + ".txt")
        observation.to_csv(filename, fmt, float_format)

    return 0
