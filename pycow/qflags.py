# -*- coding: utf-8 -*-

""" Data quality check and control functions

More detailed description.
"""

import pandas as pd


def _enhanced_sigma_test(data, n_sigma=3, is_normal=True, data_entry_window=72):
    """ Compute quality flag using enhanced sigma test

    Compute quality flags of time series according
    to sigma test (Hubbard, 2005; Shulski et al., 2014)

    n_sigma = 4 is most suitable for an extreme event analysis,
    while n_sigma = 3 is most suitable for a variability analysis
    (Shulski et al., 2014). n_sigma = 6 might be use fro precipitation.


    Parameters
    ----------
    data: pandas.Series
        Series of data for which quality flags must be computed
    n_sigma: int, default 3
        number of standard deviations by which data sample can differ from mean
    is_normal: bool, default True
        Is the data normally distributed ?
    data_entry_window: int, Default 72
        size of the data-entry window for step change and persistence tests
        (see Shulski et al., 2014)

    Notes
    -----
    The following one-liners are equivalent:
    * data.resample('M').apply(physical_threshold)
    * data.groupby(pd.Grouper(freq='M')).apply(physical_threshold)


    References
    ----------
    Hubbard, K. G., Goddard, S., Sorensen, W. D., Wells, N., & Osugi, T. T. (2005).
    Performance of quality assurance procedures for an applied climate information system.
    Journal of Atmospheric and Oceanic Technology, 22(1),105–112.
    https://doi.org/10.1175/JTECH-1657.1

    Shulski, M. D., You, J., Krieger, J. R., Baule, W., Zhang, J., Zhang, X., & Horowitz,
    W. (2014). Quality assessment of meteorological data for the Beaufort and Chukchi sea
    coastal region using automated routines. Arctic, 67(1), 104–112.
    https://doi.org/10.14430/arctic4367

    Returns
    -------
    Quality flag: pandas.Series
        * -1 = missing value
        * 0 = failure (did not pass threshold test)
        * 1 = suspect (did not pass step change test)
        * 2 = warning (persistence check failed)
        * 3 = nominal value

    """

    def physical_threshold(value):
        time_step = value.index.month[0]
        if is_normal:
            return (value > mean_daily_min[time_step] - n_sigma * std_daily_min[time_step]) & (
                value < mean_daily_max[time_step] + n_sigma * std_daily_max[time_step])
        else:
            return value < mean_daily_max[time_step] + n_sigma * std_daily_max[time_step]

    def step_change(diff):
        time_step = diff.index.month[0]
        if is_normal:
            return (diff > mean_rate_of_change[time_step] - n_sigma * std_rate_of_change[
                time_step]) & (diff < mean_rate_of_change[time_step] + n_sigma *
                               std_rate_of_change[time_step])
        else:
            return diff < mean_rate_of_change[time_step] + n_sigma * std_rate_of_change[time_step]

    def persistence_check(std):
        time_step = std.index.month[0]
        return (std > mean_std[time_step] - n_sigma * std_mean_std[time_step]) & (std < mean_std[
            time_step] + n_sigma * std_mean_std[time_step])

    # Daily min and max
    daily_max = data.resample('D').max()
    daily_min = data.resample('D').min()
    mean_daily_max = daily_max.groupby(daily_max.index.month).mean()
    mean_daily_min = daily_min.groupby(daily_min.index.month).mean()
    std_daily_max = daily_max.groupby(daily_max.index.month).std()
    std_daily_min = daily_min.groupby(daily_min.index.month).std()

    # Rate of change
    mean_rate_of_change = data.diff().groupby(data.index.month).mean()
    std_rate_of_change = data.diff().groupby(data.index.month).std()

    # Data standard deviation over each month (persistence check)
    data_std = data.resample('M').std()
    mean_std = data_std.groupby(data_std.index.month).mean()
    std_mean_std = data_std.groupby(data_std.index.month).std()
    period_std = data.resample(f"{data_entry_window}{data.index.freqstr}").std()

    # Compute flags
    q_flags = pd.Series(data=0, index=data.index)
    missing_data_flag = data.isna()
    physical_threshold_flag = data.groupby(data.index.month).apply(physical_threshold)
    step_change_flag = data.diff().groupby(data.index.month).apply(step_change)
    persistence_check_flag = period_std.groupby(period_std.index.month).apply(
        persistence_check).resample(f"{data.index.freqstr}").pad()

    q_flags[physical_threshold_flag] = 1
    q_flags[physical_threshold_flag & step_change_flag] = 2
    q_flags[physical_threshold_flag & step_change_flag & persistence_check_flag] = 3
    q_flags[missing_data_flag] = -1

    return q_flags
